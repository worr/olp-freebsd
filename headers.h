#define _KERNEL
#include <sys/types.h>
#include <sys/queue.h>
#include <sys/param.h>

#include <sys/kernel.h>
#include <sys/module.h>
#include <sys/socket.h>

#include <net/if.h>
#include <net/if_var.h>
#include <net/if_media.h>
#include <net/if_types.h>
#include <net/iso88025.h>

#include <machine/bus.h>
#include <machine/resource.h>
#include <sys/bus.h>
#include <sys/rman.h>

#include <dev/pci/pcireg.h>

#include <sys/kobj.h>
#include "pci_if.h"
#include "bus_if.h"
#include "device_if.h"
