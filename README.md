# FreeBSD olympic driver

This is a driver for the IBM Olympic 100/16/4 Token ring cards

Based largely off olympic drivers from Linux, the old oltr driver in FreeBSD,
and the FreeBSD token ring project's stub for olympic.

This also serves as a testbed for writing FreeBSD kernel modules in rust.

## Dependencies

* rust
* [rust-bindgen](https://github.com/crabtw/rust-bindgen)
