// Let's implement the KOBJ PCI interface

use core::mem;
use core::ptr::Unique;
use core::result::Result;
use core::result::Result::{Ok,Err};
use gen::*;
use kern::stubs;
use libc::c_int;
use olympic::OlpSoftc;
use util::CString;

pub type Device = device_t;

pub trait PciDevice {
    fn get_device(&self) -> c_int;
    fn get_vendor(&self) -> c_int;
    fn set_desc(&self, desc: &str);
    fn enable_busmaster(&self) -> Result<(), c_int>;
    fn get_softc(&self) -> Unique<OlpSoftc>;
}

impl PciDevice for Device {
    fn get_device(&self) -> c_int {
        unsafe {
            stubs::pci_get_device(*self)
        }
    }

    fn get_vendor(&self) -> c_int {
        unsafe {
            stubs::pci_get_vendor(*self)
        }
    }

    fn set_desc(&self, desc: &str) {
        let cstr_desc = CString::new(desc);
        unsafe {
            device_set_desc(*self, cstr_desc.to_cstr());
        }
    }

    fn enable_busmaster(&self) -> Result<(), c_int> {
        let ret = unsafe {
            stubs::my_pci_enable_busmaster(*self)
        };

        match ret {
            0 => { Ok(()) }
            _ => { Err(ret) }
        }
    }

    fn get_softc(&self) -> Unique<OlpSoftc> {
        unsafe {
            Unique::new({
                let ret = device_get_softc(*self);
                mem::transmute(ret)
            })
        }
    }
}
