use core::ptr::Unique;
use gen::{if_alloc,Struct_ifnet};

pub type IfNet = Struct_ifnet;

pub fn new(media_type: u8) -> Unique<IfNet> {
    unsafe { Unique::new(if_alloc(media_type)) }
}
