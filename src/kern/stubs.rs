// Some functions defined by the KOBJ framework aren't caught by bindgen

use gen::*;
use libc::c_int;

extern "C" {
    pub fn pci_get_device(dev: device_t) -> c_int;
    pub fn pci_get_vendor(dev: device_t) -> c_int;
    pub fn my_pci_enable_busmaster(dev: device_t) -> c_int;
}
