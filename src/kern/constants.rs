use libc::c_int;

pub const BUS_PROBE_DEFAULT: c_int = -20;
pub const ENXIO: c_int = 6;
pub const IFT_ISO88025: u8 = 0x9;
