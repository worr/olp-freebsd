#![feature(core_str_ext)]
#![feature(lang_items)]
#![feature(libc)]
#![feature(no_std)]
#![feature(unique)]
#![no_std]

extern crate libc;
extern crate rlibc;

mod gen;
mod kern;
mod olympic;
mod util;

use core::result::Result::Err;
use kern::constants::*;
use kern::ifnet;
use kern::pci;
use kern::pci::PciDevice;
use libc::c_int;
use olympic::constants::*;
use util::print_str;

#[no_mangle]
pub extern fn rust_olp_probe(dev: pci::Device) -> c_int {
    if dev.get_vendor() == OLYMPIC_VENDOR_ID &&
       dev.get_device() == OLYMPIC_DEVICE_ID {
        dev.set_desc(OLYMPIC_DEVICE_DESC);
        print_str("Discovered Olympic Token Ring Card\n\0");
        return BUS_PROBE_DEFAULT;
    }

    return ENXIO;
}

#[no_mangle]
pub extern fn rust_olp_attach(dev: pci::Device) -> libc::c_int {
    let mut sc = dev.get_softc();
    unsafe { sc.get_mut().olp_ifp = ifnet::new(IFT_ISO88025) };

    if let Err(res) = dev.enable_busmaster() {
        print_str("Couldn't enable busmaster\0");
        return 1;
    }

    return 0;
}

#[no_mangle]
pub extern fn rust_olp_detach(dev: pci::Device) -> libc::c_int {
    return 0;
}

#[no_mangle]
pub extern fn rust_olp_shutdown(dev: pci::Device) -> libc::c_int {
    return 0;
}

#[lang = "stack_exhausted"] extern fn stack_exhausted() {}
#[lang = "eh_personality"] extern fn eh_personality() {}
#[lang = "panic_fmt"] fn panic_fmt() -> ! { loop {} }
