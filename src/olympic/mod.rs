pub mod constants;

use core::ptr::Unique;
use gen::*;
use kern::ifm::IfMedia;
use kern::ifnet::IfNet;
use kern::pci::Device;

// This must match the C definition in olympic.h
#[repr(C)]
pub struct OlpSoftc {
    pub olp_ifp: Unique<IfNet>,
    pub olp_dev: Device,
    pub ifmedia: Unique<IfMedia>,
    pub mac_addr: [uint8_t; 6],
}
