use libc::c_int;

pub const OLYMPIC_VENDOR_ID: c_int = 0x1014;
pub const OLYMPIC_DEVICE_ID: c_int = 0x003e;
pub const OLYMPIC_DEVICE_DESC: &'static str = "IBM 100/16/4 Token ring controller\0";
