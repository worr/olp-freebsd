/*
 * Copyright (c) 2014 William Orr <will@worrbase.com>. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.  Redistributions in binary form must
 * reproduce the above copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided with the
 * distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Defines taken from Linux olympic driver */
#define IBM_VENDOR_ID             0x1014
#define OLYMPIC_DEVICE_ID         0x003e
#define OLYMPIC_DEVICE_DESC       "IBM 100/16/4 Token ring controller"
#define CID 0x4e

#define BCTL 0x70
#define BCTL_SOFTRESET (1<<15)
#define BCTL_MIMREB (1<<6)
#define BCTL_MODE_INDICATOR (1<<5)

#define GPR 0x4a
#define GPR_OPTI_BF (1<<6)
#define GPR_NEPTUNE_BF (1<<4) 
#define GPR_AUTOSENSE (1<<2)
#define GPR_16MBPS (1<<3) 

#define PAG 0x85
#define LBC 0x8e

#define LISR 0x10
#define LISR_SUM 0x14
#define LISR_RWM 0x18

#define LISR_LIE (1<<15)
#define LISR_SLIM (1<<13)
#define LISR_SLI (1<<12)
#define LISR_PCMSRMASK (1<<11)
#define LISR_PCMSRINT (1<<10)
#define LISR_WOLMASK (1<<9)
#define LISR_WOL (1<<8)
#define LISR_SRB_CMD (1<<5)
#define LISR_ASB_REPLY (1<<4)
#define LISR_ASB_FREE_REQ (1<<2)
#define LISR_ARB_FREE (1<<1)
#define LISR_TRB_FRAME (1<<0)

#define SISR 0x20
#define SISR_SUM 0x24
#define SISR_RWM 0x28
#define SISR_RR 0x2C
#define SISR_RESMASK 0x30
#define SISR_MASK 0x54
#define SISR_MASK_SUM 0x58
#define SISR_MASK_RWM 0x5C

#define SISR_TX2_IDLE (1<<31)
#define SISR_TX2_HALT (1<<29)
#define SISR_TX2_EOF (1<<28)
#define SISR_TX1_IDLE (1<<27)
#define SISR_TX1_HALT (1<<25)
#define SISR_TX1_EOF (1<<24)
#define SISR_TIMEOUT (1<<23)
#define SISR_RX_NOBUF (1<<22)
#define SISR_RX_STATUS (1<<21)
#define SISR_RX_HALT (1<<18)
#define SISR_RX_EOF_EARLY (1<<16)
#define SISR_MI (1<<15)
#define SISR_PI (1<<13)
#define SISR_ERR (1<<9)
#define SISR_ADAPTER_CHECK (1<<6)
#define SISR_SRB_REPLY (1<<5)
#define SISR_ASB_FREE (1<<4)
#define SISR_ARB_CMD (1<<3)
#define SISR_TRB_REPLY (1<<2)

#define EISR 0x34
#define EISR_RWM 0x38
#define EISR_MASK 0x3c
#define EISR_MASK_OPTIONS 0x001FFF7F

#define LAPA 0x60
#define LAPWWO 0x64
#define LAPWWC 0x68
#define LAPCTL 0x6C
#define LAIPD 0x78
#define LAIPDDINC 0x7C

#define TIMER 0x50

#define CLKCTL 0x74
#define CLKCTL_PAUSE (1<<15) 

#define PM_CON 0x4

#define BMCTL_SUM 0x40
#define BMCTL_RWM 0x44
#define BMCTL_TX2_DIS (1<<30) 
#define BMCTL_TX1_DIS (1<<26) 
#define BMCTL_RX_DIS (1<<22) 

#define BMASR 0xcc

#define RXDESCQ 0x90
#define RXDESCQCNT 0x94
#define RXCDA 0x98
#define RXENQ 0x9C
#define RXSTATQ 0xA0
#define RXSTATQCNT 0xA4
#define RXCSA 0xA8
#define RXCLEN 0xAC
#define RXHLEN 0xAE

#define TXDESCQ_1 0xb0
#define TXDESCQ_2 0xd0
#define TXDESCQCNT_1 0xb4
#define TXDESCQCNT_2 0xd4
#define TXCDA_1 0xb8
#define TXCDA_2 0xd8
#define TXENQ_1 0xbc
#define TXENQ_2 0xdc
#define TXSTATQ_1 0xc0
#define TXSTATQ_2 0xe0
#define TXSTATQCNT_1 0xc4
#define TXSTATQCNT_2 0xe4
#define TXCSA_1 0xc8
#define TXCSA_2 0xe8
/* Cardbus */
#define FERMASK 0xf4
#define FERMASK_INT_BIT (1<<15)

#define OLYMPIC_IO_SPACE 256

#define SRB_COMMAND_SIZE 50

#define OLYMPIC_MAX_ADAPTERS 8 /* 0x08 __MODULE_STRING can't hand 0xnn */

/* Defines for LAN STATUS CHANGE reports */
#define LSC_SIG_LOSS 0x8000
#define LSC_HARD_ERR 0x4000
#define LSC_SOFT_ERR 0x2000
#define LSC_TRAN_BCN 0x1000
#define LSC_LWF      0x0800
#define LSC_ARW      0x0400
#define LSC_FPE      0x0200
#define LSC_RR       0x0100
#define LSC_CO       0x0080
#define LSC_SS       0x0040
#define LSC_RING_REC 0x0020
#define LSC_SR_CO    0x0010
#define LSC_FDX_MODE 0x0004

/* Defines for OPEN ADAPTER command */

#define OPEN_ADAPTER_EXT_WRAP (1<<15)
#define OPEN_ADAPTER_DIS_HARDEE (1<<14)
#define OPEN_ADAPTER_DIS_SOFTERR (1<<13)
#define OPEN_ADAPTER_PASS_ADC_MAC (1<<12)
#define OPEN_ADAPTER_PASS_ATT_MAC (1<<11)
#define OPEN_ADAPTER_ENABLE_EC (1<<10)
#define OPEN_ADAPTER_CONTENDER (1<<8)
#define OPEN_ADAPTER_PASS_BEACON (1<<7)
#define OPEN_ADAPTER_ENABLE_FDX (1<<6)
#define OPEN_ADAPTER_ENABLE_RPL (1<<5)
#define OPEN_ADAPTER_INHIBIT_ETR (1<<4)
#define OPEN_ADAPTER_INTERNAL_WRAP (1<<3)
#define OPEN_ADAPTER_USE_OPTS2 (1<<0)

#define OPEN_ADAPTER_2_ENABLE_ONNOW (1<<15)

/* Defines for SRB Commands */

#define SRB_ACCESS_REGISTER 0x1f
#define SRB_CLOSE_ADAPTER 0x04
#define SRB_CONFIGURE_BRIDGE 0x0c
#define SRB_CONFIGURE_WAKEUP_EVENT 0x1a
#define SRB_MODIFY_BRIDGE_PARMS 0x15
#define SRB_MODIFY_OPEN_OPTIONS 0x01
#define SRB_MODIFY_RECEIVE_OPTIONS 0x17
#define SRB_NO_OPERATION 0x00
#define SRB_OPEN_ADAPTER 0x03
#define SRB_READ_LOG 0x08
#define SRB_READ_SR_COUNTERS 0x16
#define SRB_RESET_GROUP_ADDRESS 0x02
#define SRB_SAVE_CONFIGURATION 0x1b
#define SRB_SET_BRIDGE_PARMS 0x09
#define SRB_SET_BRIDGE_TARGETS 0x10
#define SRB_SET_FUNC_ADDRESS 0x07
#define SRB_SET_GROUP_ADDRESS 0x06
#define SRB_SET_GROUP_ADDR_OPTIONS 0x11
#define SRB_UPDATE_WAKEUP_PATTERN 0x19

/* Clear return code */

#define OLYMPIC_CLEAR_RET_CODE 0xfe 

/* ARB Commands */
#define ARB_RECEIVE_DATA 0x81
#define ARB_LAN_CHANGE_STATUS 0x84
/* ASB Response commands */

#define ASB_RECEIVE_DATA 0x81


/* Olympic defaults for buffers */

#define OLYMPIC_RX_RING_SIZE 16 /* should be a power of 2 */
#define OLYMPIC_TX_RING_SIZE 8 /* should be a power of 2 */

#define PKT_BUF_SZ 4096 /* Default packet size */

struct olp_softc {
	struct ifnet            *olp_ifp;        /* interface info */
	device_t                olp_dev;         /* device info */
	struct ifmedia          ifmedia;        /* media info */
	u_int8_t                mac_addr[6];
};

